$(function() {
  let markdown = window.location.pathname;

  if (window.location.pathname.slice(-1) == "/") {
    markdown = window.location.pathname.slice(0, -1);
  }

  $.get(`${markdown}.md`, function(data) {
    let converter = new showdown.Converter();
    $("#panel").html(converter.makeHtml(data));
  });
});
