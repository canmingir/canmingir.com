const https = require("https");
const fs = require("fs");
const express = require("express");
const app = express();

app.get("/", (req, res) => res.redirect("/articles/"));
app.get("*.*", (req, res) => res.sendFile(req.url.slice(1), { root: "/opt/canmingir.com/" }));
app.get("*", (req, res) => res.sendFile("template.html", { root: "/opt/canmingir.com/" }));

let cert = fs.readFileSync("/opt/letsencrypt/cert.pem", "utf8");
let key = fs.readFileSync("/opt/letsencrypt/key.pem", "utf8");
https.createServer({ key, cert }, app).listen(443);

const http = express();
let host = "https://canmingir.com";
http.get("*", (req, res) => res.redirect(host + req.url));
http.listen(80);
